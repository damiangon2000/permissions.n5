# Permission.N5

Para poder ejecutar correctamente todo solo se debe correr el siguiente comando:

<code> docker-compuse up --build </code>

Links:

- La documentación de la api se encuentra en [api/docs](http://localhost:5000/swagger/index.html) 
- La aplicacion web en reatjs se encuentra en [app](http://localhost:3000)
