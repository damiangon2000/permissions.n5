﻿using Ardalis.Result;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.UseCases.Permissions;
using Permissions.N5.UseCases.Permissions.Request;
using Permissions.N5.Web;
using Xunit;

namespace Permissions.N5.UnitTests.UseCases.Permissions;

public class RequestPermissionHandlerHandle
{
    private readonly RequestPermissionHandler _handler;
    private readonly IRequestPermissionService _permissionService = Substitute.For<IRequestPermissionService>();
    private readonly IElasticSearchService _elasticService = Substitute.For<IElasticSearchService>();
    private readonly IApacheKafkaService _kakService = Substitute.For<IApacheKafkaService>();
    private readonly ILogger<RequestPermissionHandler> _logger = Substitute.For<ILogger<RequestPermissionHandler>>();

    public RequestPermissionHandlerHandle()
    {
      _handler = new RequestPermissionHandler(_permissionService, _elasticService, _kakService, _logger);
    }

    private Permission CreatePermission()
    {
      return new Permission("test", "tes", DateTime.Now) { PermissionType = SeedData.PermissionTypeVacationleave };
    }

    [Fact]
    public async Task ReturnsSuccessGivenValidName()
    {
      _permissionService.RequestPermission(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>(), Arg.Any<DateTime>())
        .Returns(Result.Success(CreatePermission()));

    var result = await _handler.Handle(new RequestPermissionCommand("test", "test", 1, DateTime.Now),
          CancellationToken.None);

        result.IsSuccess.Should().BeTrue();
    }
}
