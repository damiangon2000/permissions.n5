﻿using Ardalis.SharedKernel;
using Permissions.N5.Core.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Core.PermissionTypeAggregate;
using Permissions.N5.Web;
using Xunit;

namespace Permissions.N5.UnitTests.Core.Services;

public class RequestPermissionServiceTest
{
    private readonly IRepository<Permission> _repository = Substitute.For<IRepository<Permission>>();
    private readonly IRepository<PermissionType> _repPermissionType = Substitute.For<IRepository<PermissionType>>();
    private readonly IMediator _mediator = Substitute.For<IMediator>();
    private readonly ILogger<RequestPermissionService> _logger = Substitute.For<ILogger<RequestPermissionService>>();

    private readonly IRequestPermissionService _service;

    public RequestPermissionServiceTest()
    {
        _service = new RequestPermissionService(_repPermissionType, _repository, _mediator, _logger);
    }

    [Fact]
    public async Task ReturnsOk()
    {
      _repPermissionType.GetByIdAsync(Arg.Any<int>(), Arg.Any<CancellationToken>())!
        .Returns(Task.FromResult(SeedData.PermissionTypeMarriagelicense));
      
      var result = await _service.RequestPermission("test", "test", 1, DateTime.Now);
      Assert.Equal(Ardalis.Result.ResultStatus.Ok, result.Status);
    }
    
    [Fact]
    public async Task ReturnsInvalidGivenPermissionTypeInvalid()
    {
      _repPermissionType.GetByIdAsync(Arg.Any<int>(), Arg.Any<CancellationToken>())!
        .ReturnsNull();
      
      var result = await _service.RequestPermission("test", "test", 1, DateTime.Now);
      Assert.Equal(Ardalis.Result.ResultStatus.Invalid, result.Status);
    }
}
