﻿using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Web;
using Xunit;

namespace Permissions.N5.IntegrationTests.Data;

public class EfRepositoryAdd : BaseEfRepoTestFixture
{
    [Fact]
    public async Task AddsPermissionAndSetsId()
    {
      var testPermissionForename = "testForename";
      var testPermissionSurname = "testForename";
      var permissionGranted = DateTime.Now;
      
      var repository = GetRepository();
      var permission = new Permission(testPermissionForename, testPermissionSurname, permissionGranted)
      {
        PermissionType = SeedData.PermissionTypeVacationleave
      };

      await repository.AddAsync(permission);

      var newPermission = (await repository.ListAsync())
        .FirstOrDefault();

      Assert.Equal(testPermissionForename, newPermission?.EmployeeForename);
      Assert.Equal(testPermissionSurname, newPermission?.EmployeeSurname);
      Assert.Equal(SeedData.PermissionTypeVacationleave, newPermission?.PermissionType);
      Assert.True(newPermission?.Id > 0);
    }
}

