﻿using Permissions.N5.Infrastructure.Data;
using Ardalis.SharedKernel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Web;

namespace Permissions.N5.IntegrationTests.Data;

public abstract class BaseEfRepoTestFixture
{
    protected AppDbContext _dbContext;

    protected BaseEfRepoTestFixture()
    {
        var options = CreateNewContextOptions();
        var _fakeEventDispatcher = Substitute.For<IDomainEventDispatcher>();

        _dbContext = new AppDbContext(options, _fakeEventDispatcher);
        SeedData.PopulateTestData(_dbContext);
    }

    protected static DbContextOptions<AppDbContext> CreateNewContextOptions()
    {
        var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();
        
        var builder = new DbContextOptionsBuilder<AppDbContext>();
        builder.UseInMemoryDatabase("permissiondb")
            .UseInternalServiceProvider(serviceProvider);
        
        return builder.Options;
    }

    protected EfRepository<Permission> GetRepository()
    {
        return new EfRepository<Permission>(_dbContext);
    }
}
