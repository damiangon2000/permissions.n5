﻿using Ardalis.GuardClauses;
using Ardalis.SharedKernel;


namespace Permissions.N5.Core.PermissionTypeAggregate;

public class PermissionType : EntityBase, IAggregateRoot
{
  public string Description { get; set; }

  public PermissionType(string description)
  {
     Description =  Guard.Against.NullOrEmpty(description, nameof(description));
  }
}
