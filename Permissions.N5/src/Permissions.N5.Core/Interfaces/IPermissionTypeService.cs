using Ardalis.Result;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Core.Interfaces;

public interface IPermissionTypeService
{
  public Task<Result<List<PermissionType>>> ListAll();
}
