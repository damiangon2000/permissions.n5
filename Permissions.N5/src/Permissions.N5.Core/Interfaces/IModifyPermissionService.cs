﻿using Ardalis.Result;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Core.Interfaces;

public interface IModifyPermissionService
{
  public Task<Result<Permission>> ModifyPermission(int permissionId, string newEmployeForeName, string newEmployeeSurname, int newPermissionTypeId, DateTime newPermissionGranted);
}
