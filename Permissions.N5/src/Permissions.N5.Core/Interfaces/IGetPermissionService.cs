﻿using Ardalis.Result;
using Permissions.N5.Core.PermissionAggregate;

namespace Permissions.N5.Core.Interfaces;

public interface IGetPermissionService
{
  public Task<Result<IEnumerable<Permission>>> ListAll();
  public Task<Result<Permission>> GetById(int id);

}
