﻿using Ardalis.Result;
using Permissions.N5.Core.PermissionAggregate;

namespace Permissions.N5.Core.Interfaces;

public interface IRequestPermissionService
{
  public Task<Result<Permission>> RequestPermission(string employeeForename, string employeeSurname, int permissionTypeId, DateTime permissionDate);
  
}
