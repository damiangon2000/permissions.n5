﻿using Autofac;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.Services;

namespace Permissions.N5.Core;

public class DefaultCoreModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
      
      builder.RegisterType<GetPermissionService>()
        .As<IGetPermissionService>().InstancePerLifetimeScope();
      builder.RegisterType<RequestPermissionService>()
        .As<IRequestPermissionService>().InstancePerLifetimeScope();
      builder.RegisterType<ModifyPermissionService>()
        .As<IModifyPermissionService>().InstancePerLifetimeScope();
      builder.RegisterType<PermissionTypeService>()
        .As<IPermissionTypeService>().InstancePerLifetimeScope();
    }
}
