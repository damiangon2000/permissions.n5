using Ardalis.Result;
using Ardalis.SharedKernel;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Core.Services;

public class PermissionTypeService : IPermissionTypeService
{
  private readonly IReadRepository<PermissionType> _repository;
  private readonly ILogger<PermissionTypeService> _logger;


  public PermissionTypeService(IReadRepository<PermissionType> repository, ILogger<PermissionTypeService> logger)
  {
    _repository = repository;
    _logger = logger;
  }

  public async Task<Result<List<PermissionType>>> ListAll()
  {
    try
    {
      var permissionTypes = await _repository.ListAsync();
      return Result.Success(permissionTypes);
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(PermissionTypeService));
      return Result<List<PermissionType>>.Error(e.Message);
    }
  }
}
