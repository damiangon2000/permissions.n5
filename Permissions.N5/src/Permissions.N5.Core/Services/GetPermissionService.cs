﻿using Ardalis.Result;
using Ardalis.SharedKernel;
using MediatR;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Core.PermissionAggregate.Events;
using Permissions.N5.Core.PermissionAggregate.Specifications;

namespace Permissions.N5.Core.Services;

public class GetPermissionService : IGetPermissionService
{
  private readonly IReadRepository<Permission> _readRepository;
  private readonly IMediator _mediator;
  private readonly ILogger<GetPermissionService> _logger;

  public GetPermissionService(IReadRepository<Permission> readRepository, IMediator mediator, ILogger<GetPermissionService> logger)
  {
    _readRepository = readRepository;
    _mediator = mediator;
    _logger = logger;
  }

  public async Task<Result<IEnumerable<Permission>>> ListAll()
  {
    try
    {
      var permissionWithIncludeSpec = new PermissionsWithIncludeSpec();
      var permissions = await _readRepository.ListAsync(permissionWithIncludeSpec);
      
      var domainEvent = new GetPermissionsEvent();
      await _mediator.Publish(domainEvent);
      
      return Result<IEnumerable<Permission>>.Success(permissions);
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(GetPermissionService));
      return Result<IEnumerable<Permission>>.Error(e.Message);
    }
   
  }

  public async Task<Result<Permission>> GetById(int id)
  {
    try
    {
      var permissionByIdSpec = new PermissionByIdWithIncludeSpec(id);
      var dbPermission = await _readRepository.SingleOrDefaultAsync(permissionByIdSpec);
      if (dbPermission != null)
      {
        return Result.Success(dbPermission);
      }

      var errors = new List<ValidationError>
      {
        new() { Identifier = nameof(id), ErrorMessage = $"El {nameof(id)} no existe." }
      };
      return Result<Permission>.Invalid(errors);
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(ModifyPermissionService));
      return Result<Permission>.Error(e.Message);
    }
    
  }
}
