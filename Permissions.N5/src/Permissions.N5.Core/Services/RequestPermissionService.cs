﻿using Ardalis.Result;
using Ardalis.SharedKernel;
using MediatR;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Core.PermissionAggregate.Events;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Core.Services;

public class RequestPermissionService : IRequestPermissionService
{
  private readonly IRepository<Permission> _repPermission;
  private readonly IRepository<PermissionType> _repPermissionType;
  private readonly IMediator _mediator;
  private readonly ILogger<RequestPermissionService> _logger;

  public RequestPermissionService(IRepository<PermissionType> repPermissionType, 
    IRepository<Permission> repPermission, IMediator mediator, ILogger<RequestPermissionService> logger)
  {
    _repPermissionType = repPermissionType;
    _repPermission = repPermission;
    _mediator = mediator;
    _logger = logger;
  }

  public async Task<Result<Permission>> RequestPermission(string employeeForename, string employeeSurname,
    int permissionTypeId, DateTime permissionDate)
  {
    try
    {
      var permissionType = await _repPermissionType.GetByIdAsync(permissionTypeId);
      if (permissionType == null) 
      {
        var errors = new List<ValidationError>
        {
          new() { Identifier = nameof(permissionTypeId), ErrorMessage = $"El {nameof(permissionTypeId)} no existe." }
        };
        return Result<Permission>.Invalid(errors);
      }
    
      var newPermission = new Permission(employeeForename, employeeSurname, permissionDate)
      {
        PermissionType = permissionType
      };

      await _repPermission.AddAsync(newPermission);
      await _repPermission.SaveChangesAsync();
    
      var domainEvent = new PermissionRequestedEvent(newPermission);
      await _mediator.Publish(domainEvent);
      return Result.Success(newPermission);
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(RequestPermissionService));
      return Result<Permission>.Error(e.Message);
    }
    
  }
}
