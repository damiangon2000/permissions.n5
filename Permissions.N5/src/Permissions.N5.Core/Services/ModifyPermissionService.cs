using Ardalis.Result;
using Ardalis.SharedKernel;
using MediatR;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Core.PermissionAggregate.Events;
using Permissions.N5.Core.PermissionAggregate.Specifications;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Core.Services;

public class ModifyPermissionService : IModifyPermissionService
{
  private readonly IRepository<Permission> _repPermission;
  private readonly IRepository<PermissionType> _repPermissionType;
  private readonly IMediator _mediator;
  private readonly ILogger<ModifyPermissionService> _logger;

  public ModifyPermissionService(IRepository<Permission> repPermission, IRepository<PermissionType> repPermissionType, IMediator mediator, ILogger<ModifyPermissionService> logger)
  {
    _repPermission = repPermission;
    _repPermissionType = repPermissionType;
    _mediator = mediator;
    _logger = logger;
  }

  public async Task<Result<Permission>> ModifyPermission(int permissionId, string newEmployeForeName, string newEmployeeSurname, int newPermissionTypeId, DateTime newPermissionGranted)
  {
    
    try
    {
      var newPermissionType = await _repPermissionType.GetByIdAsync(newPermissionTypeId);
      if (newPermissionType == null) 
      {
        var errors = new List<ValidationError>
        {
          new() { Identifier = nameof(newPermissionTypeId), ErrorMessage = $"El {nameof(newPermissionTypeId)} no existe." }
        };
        return Result<Permission>.Invalid(errors);
      }

      var permissionByIdSpec = new PermissionByIdWithIncludeSpec(permissionId);
      var dbPermission = await _repPermission.SingleOrDefaultAsync(permissionByIdSpec);
      if (dbPermission == null) 
      {
        var errors = new List<ValidationError>
        {
          new() { Identifier = nameof(permissionId), ErrorMessage = $"El {nameof(permissionId)} no existe." }
        };
        return Result<Permission>.Invalid(errors);
      }

      dbPermission.PermissionType = newPermissionType;
      dbPermission.EmployeeSurname = newEmployeeSurname;
      dbPermission.EmployeeForename = newEmployeForeName;
      dbPermission.PermissionGranted = newPermissionGranted;

      await _repPermission.UpdateAsync(dbPermission);
      await _repPermission.SaveChangesAsync();
    
      var domainEvent = new ModifyPermissionEvent(dbPermission);
      await _mediator.Publish(domainEvent);
      return Result.Success(dbPermission);
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(ModifyPermissionService));
      return Result<Permission>.Error(e.Message);
    }
  }
}
