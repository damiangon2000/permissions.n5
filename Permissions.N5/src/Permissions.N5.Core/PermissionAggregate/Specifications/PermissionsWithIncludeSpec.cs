using Ardalis.Specification;

namespace Permissions.N5.Core.PermissionAggregate.Specifications;

public class PermissionsWithIncludeSpec : Specification<Permission>
{
  public PermissionsWithIncludeSpec()
  {
    Query
      .Include(it => it.PermissionType);
  }
}
