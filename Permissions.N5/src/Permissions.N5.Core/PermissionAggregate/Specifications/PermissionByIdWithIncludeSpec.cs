using Ardalis.Specification;

namespace Permissions.N5.Core.PermissionAggregate.Specifications;

public class PermissionByIdWithIncludeSpec : Specification<Permission>, ISingleResultSpecification<Permission>
{
  public PermissionByIdWithIncludeSpec(int id)
  {
    Query
      .Include(it => it.PermissionType)
      .Where(it => it.Id == id);
  }
  
}
