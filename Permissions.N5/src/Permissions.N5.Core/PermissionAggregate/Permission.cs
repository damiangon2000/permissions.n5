﻿using Ardalis.GuardClauses;
using Ardalis.SharedKernel;
using Permissions.N5.Core.PermissionTypeAggregate;


namespace Permissions.N5.Core.PermissionAggregate;

public class Permission : EntityBase, IAggregateRoot
{
  public string EmployeeForename { get; set; }
  public string EmployeeSurname { get; set; }
  public DateTime PermissionGranted { get; set; }

  public PermissionType PermissionType { get; set; } = null!;


  public Permission(string employeeForename, string employeeSurname, DateTime permissionGranted)
  {
    EmployeeForename = Guard.Against.NullOrEmpty(employeeForename, nameof(employeeForename));
    EmployeeSurname = Guard.Against.NullOrEmpty(employeeSurname, nameof(employeeSurname));
    PermissionGranted = permissionGranted;
  }
  
}
