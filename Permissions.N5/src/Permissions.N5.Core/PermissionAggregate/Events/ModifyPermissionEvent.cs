﻿using Ardalis.SharedKernel;

namespace Permissions.N5.Core.PermissionAggregate.Events;

public class ModifyPermissionEvent : DomainEventBase
{
  public Permission ModifiedPermission { get; set; }
  public ModifyPermissionEvent(Permission modifiedPermission)
  {
    ModifiedPermission = modifiedPermission;
  }
}
