﻿
using Ardalis.SharedKernel;

namespace Permissions.N5.Core.PermissionAggregate.Events;

public class PermissionRequestedEvent : DomainEventBase
{
  public Permission PermissionRequested { get; set; }
  public PermissionRequestedEvent(Permission permissionRequested)
  {
    PermissionRequested = permissionRequested;
  }
}
