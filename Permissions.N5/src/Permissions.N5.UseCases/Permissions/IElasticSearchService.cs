﻿using Permissions.N5.Core.PermissionAggregate;

namespace Permissions.N5.UseCases.Permissions;

public interface IElasticSearchService
{
  Task InsertDocument(Permission permission);
}
