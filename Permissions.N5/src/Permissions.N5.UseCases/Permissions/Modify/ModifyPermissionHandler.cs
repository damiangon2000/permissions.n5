﻿using Ardalis.Result;
using Ardalis.SharedKernel;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.UseCases.PermissionTypes;

namespace Permissions.N5.UseCases.Permissions.Modify;

public class ModifyPermissionHandler : ICommandHandler<ModifyPermissionCommand, Result<PermissionDTO>>
{
  private readonly IModifyPermissionService _modifyPermissionService;
  private readonly IElasticSearchService _elasticService;
  private readonly IApacheKafkaService _kafkaService;
  private readonly ILogger<ModifyPermissionHandler> _logger;
  public ModifyPermissionHandler(IModifyPermissionService modifyPermissionService, IElasticSearchService elasticService, IApacheKafkaService kafkaService, ILogger<ModifyPermissionHandler> logger)
  {
    _modifyPermissionService = modifyPermissionService;
    _elasticService = elasticService;
    _kafkaService = kafkaService;
    _logger = logger;
  }

  public async Task<Result<PermissionDTO>> Handle(ModifyPermissionCommand request, CancellationToken cancellationToken)
  {
    try
    {
      var result = await _modifyPermissionService.ModifyPermission(request.Id, request.EmployeForeName, request.EmployeeSurname,
        request.PermissionTypeId, request.PermissionGranted);

      if (!result.IsSuccess)
      {
        return result.Errors.Any() ? Result.Error(result.Errors as string[]) : Result.Invalid(result.ValidationErrors);
      }
    
      //insert into elastic
      await _elasticService.InsertDocument(result.Value);

      await _kafkaService.ProduceMessage(OperationMessageDTO.TOPIC_NAME, new OperationMessageDTO("modify"));

      return Result.Success(new PermissionDTO(result.Value.Id, result.Value.EmployeeForename,
        result.Value.EmployeeSurname, result.Value.PermissionGranted,
        new PermissionTypeDTO(result.Value.PermissionType.Id, result.Value.PermissionType.Description)));
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(ModifyPermissionHandler));
      return Result<PermissionDTO>.Error(new[] { e.Message });
    }
   
  }
}
