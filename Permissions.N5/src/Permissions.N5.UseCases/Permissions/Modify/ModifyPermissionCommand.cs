﻿using Ardalis.Result;
using Ardalis.SharedKernel;

namespace Permissions.N5.UseCases.Permissions.Modify;

public record ModifyPermissionCommand
  (int Id, string EmployeForeName, string EmployeeSurname, int PermissionTypeId, DateTime PermissionGranted) : ICommand<Result<PermissionDTO>>;
