﻿using System.Text.Json;
using Confluent.Kafka;

namespace Permissions.N5.UseCases.Permissions;

public class OperationMessageDTO
{

  public static string TOPIC_NAME = "PermissionEvents";

  public OperationMessageDTO(string name)
  {
    Id = Guid.NewGuid();
    Name = name;
  }
  
  public Guid Id { get; set; }
  public string Name { get; set; } = null!;
  public override string ToString()
  {
    return JsonSerializer.Serialize(this);
  }
}
