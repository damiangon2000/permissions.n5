using Ardalis.Result;
using Ardalis.SharedKernel;

namespace Permissions.N5.UseCases.Permissions.Get;

public record GetPermissionsQuery(int? Skip, int? Take) : IQuery<Result<IEnumerable<PermissionDTO>>>;
