﻿using Ardalis.Result;
using Ardalis.SharedKernel;

namespace Permissions.N5.UseCases.Permissions.Get;

public record GetPermissionByIdQuery(int Id) : IQuery<Result<PermissionDTO>>;
