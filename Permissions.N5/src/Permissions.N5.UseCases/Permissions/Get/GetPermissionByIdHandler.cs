﻿using Ardalis.Result;
using Ardalis.SharedKernel;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.UseCases.PermissionTypes;

namespace Permissions.N5.UseCases.Permissions.Get;

public class GetPermissionByIdHandler : IQueryHandler<GetPermissionByIdQuery, Result<PermissionDTO>>
{
  private readonly IGetPermissionService _getPermissionService;

  public GetPermissionByIdHandler(IGetPermissionService getPermissionService)
  {
    _getPermissionService = getPermissionService;
  }

  public async Task<Result<PermissionDTO>> Handle(GetPermissionByIdQuery request, CancellationToken cancellationToken)
  {
    var result = await _getPermissionService.GetById(request.Id);
    if (!result.IsSuccess)
    {
      return result.Errors.Any() ? Result.Error(result.Errors as string[]) : Result.Invalid(result.ValidationErrors);
    }

    return Result.Success(new PermissionDTO(result.Value.Id, result.Value.EmployeeForename,
      result.Value.EmployeeSurname,
      result.Value.PermissionGranted,
      new PermissionTypeDTO(result.Value.PermissionType.Id, result.Value.PermissionType.Description)));
    
  }
}



