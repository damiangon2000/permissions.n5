using Ardalis.Result;
using Ardalis.SharedKernel;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.UseCases.PermissionTypes;

namespace Permissions.N5.UseCases.Permissions.Get;

public class GetPermissionsHandler : IQueryHandler<GetPermissionsQuery, Result<IEnumerable<PermissionDTO>>>
{
  private readonly IGetPermissionService _getPermissionService;
  private readonly IApacheKafkaService _kafkaService;
  private readonly ILogger<GetPermissionsHandler> _logger;

  public GetPermissionsHandler(IGetPermissionService getPermissionService, IApacheKafkaService kafkaService, ILogger<GetPermissionsHandler> logger)
  {
    _getPermissionService = getPermissionService;
    _kafkaService = kafkaService;
    _logger = logger;
  }

  public async Task<Result<IEnumerable<PermissionDTO>>> Handle(GetPermissionsQuery request, CancellationToken cancellationToken)
  {
    try
    {
      var result = await _getPermissionService.ListAll();
      if (!result.IsSuccess)
      {
        return result.Errors.Any() ? Result.Error(result.Errors as string[]) : Result.Invalid(result.ValidationErrors);
      }

      await _kafkaService.ProduceMessage(OperationMessageDTO.TOPIC_NAME, new OperationMessageDTO("get"));
     
      return Result.Success(result.Value.Select(it => new PermissionDTO(it.Id, it.EmployeeForename, it.EmployeeSurname,
        it.PermissionGranted, new PermissionTypeDTO(it.PermissionType.Id, it.PermissionType.Description))));
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(GetPermissionsHandler));
      return Result<IEnumerable<PermissionDTO>>.Error(new[] { e.Message });
    }
     
     
  }
}
