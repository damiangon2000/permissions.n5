﻿using Permissions.N5.UseCases.PermissionTypes;

namespace Permissions.N5.UseCases.Permissions;

public record PermissionDTO(int Id, string EmployeeForename, string EmployeeSurname, DateTime PermissionGranted, PermissionTypeDTO PermissionType);

