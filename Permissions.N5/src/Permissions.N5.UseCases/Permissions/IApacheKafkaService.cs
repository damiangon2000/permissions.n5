﻿namespace Permissions.N5.UseCases.Permissions;

public interface IApacheKafkaService
{
  Task ProduceMessage(string topic, OperationMessageDTO operationMessage);
}
