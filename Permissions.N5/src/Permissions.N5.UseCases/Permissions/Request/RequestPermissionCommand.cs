using Ardalis.Result;
using Ardalis.SharedKernel;

namespace Permissions.N5.UseCases.Permissions.Request;

public record RequestPermissionCommand(string EmployeeForename, string EmployeeSurname, int PermissionTypeId, DateTime PermissionGranted ) : ICommand<Result<PermissionDTO>>;
