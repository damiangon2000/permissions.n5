using Ardalis.Result;
using Ardalis.SharedKernel;
using Microsoft.Extensions.Logging;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.UseCases.PermissionTypes;

namespace Permissions.N5.UseCases.Permissions.Request;

public class RequestPermissionHandler : ICommandHandler<RequestPermissionCommand, Result<PermissionDTO>>
{
  private readonly IRequestPermissionService _requestPermissionService;
  private readonly IElasticSearchService _elasticService;
  private readonly IApacheKafkaService _kafkaService;
  private readonly ILogger<RequestPermissionHandler> _logger;

  public RequestPermissionHandler(IRequestPermissionService requestPermissionService, IElasticSearchService elasticService, IApacheKafkaService kafkaService, ILogger<RequestPermissionHandler> logger)
  {
    _requestPermissionService = requestPermissionService;
    _elasticService = elasticService;
    _kafkaService = kafkaService;
    _logger = logger;
  }

  public async Task<Result<PermissionDTO>> Handle(RequestPermissionCommand request, CancellationToken cancellationToken)
  {
    try
    {
      var result = await _requestPermissionService.RequestPermission( request.EmployeeForename, request.EmployeeSurname, 
        request.PermissionTypeId, request.PermissionGranted);

      if (!result.IsSuccess)
      {
        return result.Errors.Any() ? Result.Error(result.Errors as string[]) : Result.Invalid(result.ValidationErrors);
      }

      //insert into elastic
      await _elasticService.InsertDocument(result.Value);

      await _kafkaService.ProduceMessage(OperationMessageDTO.TOPIC_NAME, new OperationMessageDTO("request"));
    
      return Result.Success(new PermissionDTO(result.Value.Id, result.Value.EmployeeForename,
        result.Value.EmployeeSurname, result.Value.PermissionGranted,
        new PermissionTypeDTO(result.Value.PermissionType.Id, result.Value.PermissionType.Description)));
    }
    catch (Exception e)
    {
      _logger.LogError(e, nameof(RequestPermissionHandler));
      return Result<PermissionDTO>.Error(new[] { e.Message });
    }

  }
}
