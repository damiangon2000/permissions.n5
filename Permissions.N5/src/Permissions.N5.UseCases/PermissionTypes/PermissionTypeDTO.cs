namespace Permissions.N5.UseCases.PermissionTypes;

public record PermissionTypeDTO(int Id, string Description);
