using Ardalis.Result;
using Ardalis.SharedKernel;

namespace Permissions.N5.UseCases.PermissionTypes.List;

public record ListPermissionTypesQuery(int? Skip, int? Take) : IQuery<Result<IEnumerable<PermissionTypeDTO>>>;

