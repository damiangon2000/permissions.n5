using Ardalis.Result;
using Ardalis.SharedKernel;
using Permissions.N5.Core.Interfaces;

namespace Permissions.N5.UseCases.PermissionTypes.List;

public class ListPermissionTypesHandler : IQueryHandler<ListPermissionTypesQuery, Result<IEnumerable<PermissionTypeDTO>>>
{
  private readonly IPermissionTypeService _permissionTypeService;

  public ListPermissionTypesHandler(IPermissionTypeService permissionTypeService)
  {
    _permissionTypeService = permissionTypeService;
  }

  public async Task<Result<IEnumerable<PermissionTypeDTO>>> Handle(ListPermissionTypesQuery request, CancellationToken cancellationToken)
  {
    var result = await _permissionTypeService.ListAll();
    if (!result.IsSuccess)
    {
      return Result.Error(result.Errors as string[]);
    }

    return Result.Success(result.Value.Select(it => new PermissionTypeDTO(it.Id, it.Description)));
  }
}
