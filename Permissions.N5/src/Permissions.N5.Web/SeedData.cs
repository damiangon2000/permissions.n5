﻿using Permissions.N5.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Web;

public static class SeedData
{
    public static readonly PermissionType PermissionTypeVacationleave = new("Vacation leave");
    public static readonly PermissionType PermissionTypeMarriagelicense = new("Marriage license");
    

    public static void Initialize(IServiceProvider serviceProvider)
    {
      using var dbContext = new AppDbContext(
        serviceProvider.GetRequiredService<DbContextOptions<AppDbContext>>(), null);
      
      if (dbContext.PermissionTypes.Any())
      {
        return;
      }

      PopulateTestData(dbContext);
    }

    public static void PopulateTestData(AppDbContext dbContext)
    {
        dbContext.PermissionTypes.Add(PermissionTypeVacationleave);
        dbContext.PermissionTypes.Add(PermissionTypeMarriagelicense);

        dbContext.SaveChanges();
    }
}
