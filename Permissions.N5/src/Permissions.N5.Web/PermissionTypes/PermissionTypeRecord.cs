namespace Permissions.N5.Web.PermissionTypes;

public record PermissionTypeRecord(int Id, string Description);
