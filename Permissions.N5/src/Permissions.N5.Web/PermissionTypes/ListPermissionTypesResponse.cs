namespace Permissions.N5.Web.PermissionTypes;

public class ListPermissionTypesResponse
{
  public List<PermissionTypeRecord> PermissionTypes { get; set; } = new();
}
