using FastEndpoints;
using MediatR;
using Permissions.N5.UseCases.PermissionTypes.List;
using Permissions.N5.Web.ApiModels;
using Permissions.N5.Web.Helpers;
using Permissions.N5.Web.Permissions;

namespace Permissions.N5.Web.PermissionTypes;

public class List : EndpointWithoutRequest<ApiResponse<ListPermissionTypesResponse>>
{
  private readonly IMediator _mediator;

  public List(IMediator mediator)
  {
    _mediator = mediator;
  }
  
  public override void Configure()
  {
    Get("/PermissionTypes");
    AllowAnonymous();
  }
  
  public override async Task HandleAsync(CancellationToken cancellationToken)
  {
    var result = await _mediator.Send(new ListPermissionTypesQuery(null,null), cancellationToken);

    if (!result.IsSuccess)
    {
      Response = new ApiResponse<ListPermissionTypesResponse>()
      {
        Success = false,
        Error = new ApiError
        {
          Code = result.Status.ToString(),
          Message = ErrorResponseHelper.CreateErrorsResponse(result.Errors, result.ValidationErrors)
        }
      };
      return;
    }

    Response = new ApiResponse<ListPermissionTypesResponse>
    {
      Success = true,
      Data = new ListPermissionTypesResponse
      {
        PermissionTypes = result.Value.Select(c =>
          new PermissionTypeRecord(c.Id, c.Description)).ToList()
      }
    };
  }
}
