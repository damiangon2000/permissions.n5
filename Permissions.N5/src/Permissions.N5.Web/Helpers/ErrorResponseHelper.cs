using Ardalis.Result;

namespace Permissions.N5.Web.Helpers;

public class ErrorResponseHelper 
{
  public static string CreateErrorsResponse(IEnumerable<string> errors, IEnumerable<ValidationError> validationError)
  {
    var errorsMsj = string.Join(Environment.NewLine, errors);
    errorsMsj += string.Join(Environment.NewLine, validationError.Select(e => e.ErrorMessage));

    return errorsMsj;
  }
}
