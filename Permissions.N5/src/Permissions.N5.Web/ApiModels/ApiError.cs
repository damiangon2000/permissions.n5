namespace Permissions.N5.Web.ApiModels;

public class ApiError
{

  public string? Code { get; set; }
  public string? Message { get; set; }
}

