﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Permissions.N5.Core;
using Permissions.N5.Infrastructure;
using Permissions.N5.Infrastructure.Data;
using Permissions.N5.Web;
using FastEndpoints;
using FastEndpoints.Swagger;
using Serilog;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

builder.Host.UseSerilog((_, config) => config.ReadFrom.Configuration(builder.Configuration));

builder.Services.Configure<CookiePolicyOptions>(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});

var dbHost = Environment.GetEnvironmentVariable("DB_HOST") ?? "localhost";
string? connectionString = builder.Configuration.GetConnectionString("ConnectionString")?.Replace("${DB_HOST}", dbHost);
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlServer(connectionString));
builder.Services.AddCors();
builder.Services.AddFastEndpoints();
//builder.Services.AddFastEndpointsApiExplorer();
builder.Services.SwaggerDocument(o => { o.ShortSchemaNames = true; });

builder.Host.ConfigureContainer<ContainerBuilder>(containerBuilder =>
{
    containerBuilder.RegisterModule(new DefaultCoreModule());
    containerBuilder.RegisterModule(new AutofacInfrastructureModule(builder.Environment.IsDevelopment(), builder.Configuration));
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
  app.UseDeveloperExceptionPage();
}
else
{
    app.UseDefaultExceptionHandler(); 
    app.UseHsts();
}

app.UseCors(b => b
  .AllowAnyHeader()
  .AllowAnyMethod()
  .AllowAnyOrigin()
);

app.UseFastEndpoints();
app.UseSwaggerGen();

app.UseHttpsRedirection();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    try
    {
        var context = services.GetRequiredService<AppDbContext>();
        context.Database.EnsureCreated();
        SeedData.Initialize(services);
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred seeding the DB. {exceptionMessage}", ex.Message);
    }
}

app.Run();

public partial class Program
{
}
