using FastEndpoints;
using MediatR;
using Permissions.N5.UseCases.Permissions.Get;
using Permissions.N5.Web.ApiModels;
using Permissions.N5.Web.Helpers;
using Permissions.N5.Web.PermissionTypes;

namespace Permissions.N5.Web.Permissions;

public class List : EndpointWithoutRequest<ApiResponse<ListPermissionsResponse>>
{
  private readonly IMediator _mediator;

  public List(IMediator mediator)
  {
    _mediator = mediator;
  }
  
  public override void Configure()
  {
    Get("/Permissions");
    AllowAnonymous();
    Summary(s =>
    {
      s.Summary = "Get Permissions";
      s.Description = "Get all permissions.";
    });
  }
  
  public override async Task HandleAsync(CancellationToken cancellationToken)
  {
    var result = await _mediator.Send(new GetPermissionsQuery(null,null), cancellationToken);

    if (!result.IsSuccess)
    {
      Response = new ApiResponse<ListPermissionsResponse>()
      {
        Success = false,
        Error = new ApiError
        {
          Code = result.Status.ToString(),
          Message = ErrorResponseHelper.CreateErrorsResponse(result.Errors, result.ValidationErrors)
        }
      };
      return;
    }

    Response = new ApiResponse<ListPermissionsResponse>
    {
      Success = true,
      Data = new ListPermissionsResponse
      {
        Permissions = result.Value.Select(c =>
          new PermissionRecord(c.Id, c.EmployeeForename, c.EmployeeSurname, c.PermissionGranted,
            new PermissionTypeRecord(c.PermissionType.Id, c.PermissionType.Description))).ToList()
      }
    };
  }
}
