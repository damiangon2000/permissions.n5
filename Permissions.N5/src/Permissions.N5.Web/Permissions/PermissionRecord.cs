using Permissions.N5.Web.PermissionTypes;

namespace Permissions.N5.Web.Permissions;

public record PermissionRecord(int Id, string EmployeeForename, string EmployeeSurname, DateTime PermissionGranted, PermissionTypeRecord PermissionType);
