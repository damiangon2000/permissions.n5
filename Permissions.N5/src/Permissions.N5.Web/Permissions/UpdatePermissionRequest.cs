using FastEndpoints;

namespace Permissions.N5.Web.Permissions;

public class UpdatePermissionRequest
{
  [BindFrom("PermissionId")]
  public int Id { get; set; }
  public string? EmployeeForename { get; set; }
  public string? EmployeeSurname { get; set; }
  public DateTime? PermissionGranted { get; set; }

  public int? PermissionTypeId { get; set; }
}
