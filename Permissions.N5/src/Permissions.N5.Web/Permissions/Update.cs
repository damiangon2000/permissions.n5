using FastEndpoints;
using MediatR;
using Permissions.N5.UseCases.Permissions.Modify;
using Permissions.N5.Web.ApiModels;
using Permissions.N5.Web.Helpers;
using Permissions.N5.Web.PermissionTypes;

namespace Permissions.N5.Web.Permissions;

public class Update : Endpoint<UpdatePermissionRequest, ApiResponse<UpdatePermissionResponse>>
{
  private readonly IMediator _mediator;

  public Update(IMediator mediator)
  {
    _mediator = mediator;
  }

  public override void Configure()
  {
    Put("/Permissions/{PermissionId:int}");
    AllowAnonymous();
    Summary(s =>
    {
      // XML Docs are used by default but are overridden by these properties:
      s.Summary = "Modify Permission";
      s.Description = "Modify a permission by id";
      s.ExampleRequest = new UpdatePermissionRequest
      {
        EmployeeForename = "Damian Horacio",
        PermissionGranted = DateTime.Today,
        EmployeeSurname = "Gonzalez Catan",
        PermissionTypeId = 2
      };
    });
  }
  
  public override async Task HandleAsync(
    UpdatePermissionRequest request,
    CancellationToken cancellationToken)
  {
    var result =
      await _mediator.Send(
        new ModifyPermissionCommand(request.Id, request.EmployeeForename!, request.EmployeeSurname!,
          request.PermissionTypeId!.Value, request.PermissionGranted!.Value), cancellationToken);
    
    if (!result.IsSuccess)
    {
      Response = new ApiResponse<UpdatePermissionResponse>()
      {
        Success = false,
        Error = new ApiError
        {
          Code = result.Status.ToString(),
          Message = ErrorResponseHelper.CreateErrorsResponse(result.Errors, result.ValidationErrors)
        }
      };
      return;
    }

    Response = new ApiResponse<UpdatePermissionResponse>
    {
      Success = true,
      Data = new UpdatePermissionResponse(new PermissionRecord(result.Value.Id, result.Value.EmployeeForename,
        result.Value.EmployeeSurname, result.Value.PermissionGranted,
        new PermissionTypeRecord(result.Value.PermissionType.Id, result.Value.PermissionType.Description)))
    };

  }
  
}
