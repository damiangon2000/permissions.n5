namespace Permissions.N5.Web.Permissions;

public class CreatePermissionResponse
{
  public CreatePermissionResponse(int id, string employeeForename, string employeeSurname, DateTime permissionGranted)
  {
    Id = id;
    EmployeeForename = employeeForename;
    EmployeeSurname = employeeSurname;
    PermissionGranted = permissionGranted;
  }

  public int Id { get; set; }
  public string EmployeeForename { get; set; }
  public string EmployeeSurname { get; set; } 
  public DateTime PermissionGranted { get; set; }
  
}
