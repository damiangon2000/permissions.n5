﻿namespace Permissions.N5.Web.Permissions;

public class GetPermissionByIdRequest
{
  public int PermissionId { get; set; }
}
