using FastEndpoints;
using FluentValidation;
using Permissions.N5.Infrastructure.Data.Config;

namespace Permissions.N5.Web.Permissions;

public class CreatePermissionValidator : Validator<CreatePermissionRequest>
{
  public CreatePermissionValidator()
  {
    RuleFor(x => x.EmployeeForename)
      .NotEmpty()
      .WithMessage("Forename is required.")
      .MinimumLength(2)
      .MaximumLength(DataSchemaConstants.DEFAULT_NAME_LENGTH);
    RuleFor(x => x.EmployeeSurname)
      .NotEmpty()
      .WithMessage("EmployeeSurname is required.")
      .MinimumLength(2)
      .MaximumLength(DataSchemaConstants.DEFAULT_NAME_LENGTH);

    RuleFor(x => x.PermissionGranted)
      .NotNull()
      .NotEmpty()
      .WithMessage("PermissionGranted is required.");
    
    RuleFor(x => x.PermissionTypeId)
      .NotNull()
      .NotEmpty()
      .WithMessage("PermissionTypeId is required.");

  }
}
