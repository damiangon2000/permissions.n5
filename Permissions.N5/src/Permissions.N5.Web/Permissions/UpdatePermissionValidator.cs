using FastEndpoints;
using FluentValidation;
using Permissions.N5.Infrastructure.Data.Config;

namespace Permissions.N5.Web.Permissions;

public class UpdatePermissionValidator : Validator<UpdatePermissionRequest>
{
  public UpdatePermissionValidator()
  {
    RuleFor(x => x.EmployeeForename)
      .NotEmpty()
      .WithMessage("Forename is required.")
      .MinimumLength(2)
      .MaximumLength(DataSchemaConstants.DEFAULT_NAME_LENGTH);
    RuleFor(x => x.EmployeeSurname)
      .NotEmpty()
      .WithMessage("EmployeeSurname is required.")
      .MinimumLength(2)
      .MaximumLength(DataSchemaConstants.DEFAULT_NAME_LENGTH);

    RuleFor(x => x.PermissionGranted)
      .NotNull()
      .NotEmpty()
      .WithMessage("PermissionGranted is required.");
    
    RuleFor(x => x.PermissionTypeId)
      .NotNull()
      .NotEmpty()
      .WithMessage("PermissionTypeId is required.");
    RuleFor(x => x.Id)
      .Must((args, permissionId) => args.Id == permissionId)
      .WithMessage("Route and body Ids must match; cannot update Id of an existing resource.");
  }
}
