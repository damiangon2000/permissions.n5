namespace Permissions.N5.Web.Permissions;

public class CreatePermissionRequest
{
  public string? EmployeeForename { get; set; }
  public string? EmployeeSurname { get; set; }
  public DateTime? PermissionGranted { get; set; }

  public int? PermissionTypeId { get; set; }
}
