using FastEndpoints;
using MediatR;
using Permissions.N5.UseCases.Permissions.Request;
using Permissions.N5.Web.ApiModels;
using Permissions.N5.Web.Helpers;

namespace Permissions.N5.Web.Permissions;

public class Create : Endpoint<CreatePermissionRequest, ApiResponse<CreatePermissionResponse>>
{
  private readonly IMediator _mediator;

  public Create(IMediator mediator)
  {
    _mediator = mediator;
  }

  public override void Configure()
  {
    Post("/Permissions");
    AllowAnonymous();
    Summary(s =>
    {
      // XML Docs are used by default but are overridden by these properties:
      s.Summary = "Request Permission";
      s.Description = "Create a new permission";
      s.ExampleRequest = new CreatePermissionRequest
      {
        EmployeeForename = "Damian",
        PermissionGranted = DateTime.Today,
        EmployeeSurname = "Gonzalez",
        PermissionTypeId = 1
      };
    });
  }
  
  public override async Task HandleAsync(
    CreatePermissionRequest request,
    CancellationToken cancellationToken)
  {
    var result =
      await _mediator.Send(
        new RequestPermissionCommand(request.EmployeeForename!, request.EmployeeSurname!,
          request.PermissionTypeId!.Value,
          request.PermissionGranted!.Value), cancellationToken);

    if (!result.IsSuccess)
    {
      Response = new ApiResponse<CreatePermissionResponse>()
      {
        Success = false,
        Error = new ApiError
        {
          Code = result.Status.ToString(),
          Message = ErrorResponseHelper.CreateErrorsResponse(result.Errors, result.ValidationErrors)
        }
      };
      return;
    }

    Response = new ApiResponse<CreatePermissionResponse>
    {
      Success = true,
      Data = new CreatePermissionResponse(result.Value.Id, result.Value.EmployeeForename,
        result.Value.EmployeeSurname, result.Value.PermissionGranted)
    };
  }
  
}
