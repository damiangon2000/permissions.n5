﻿
using FastEndpoints;
using FluentValidation;

namespace Permissions.N5.Web.Permissions;

public class GetPermissionByIdValidator :  Validator<GetPermissionByIdRequest>
{
  public GetPermissionByIdValidator()
  {
    RuleFor(x => x.PermissionId)
      .GreaterThan(0);
    RuleFor(x => x.PermissionId)
      .Must((args, permissionId) => args.PermissionId == permissionId)
      .WithMessage("Route and body Ids must match; cannot update Id of an existing resource.");
  }
}
