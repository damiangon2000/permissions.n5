﻿using FastEndpoints;
using MediatR;
using Permissions.N5.UseCases.Permissions.Get;
using Permissions.N5.Web.ApiModels;
using Permissions.N5.Web.Helpers;
using Permissions.N5.Web.PermissionTypes;

namespace Permissions.N5.Web.Permissions;

public class GetPermissionById : Endpoint<GetPermissionByIdRequest, ApiResponse<PermissionRecord>>
{
  private readonly IMediator _mediator;

  public GetPermissionById(IMediator mediator)
  {
    _mediator = mediator;
  }
  
  public override void Configure()
  {
    Get("/Permissions/{PermissionId:int}");
    AllowAnonymous();
    Summary(s =>
    {
      s.Summary = "Get Permission by id";
      s.Description = "Get one permission .";
    });
  }
  
  public override async Task HandleAsync(GetPermissionByIdRequest request,
    CancellationToken cancellationToken)
  {
    var result = await _mediator.Send(new GetPermissionByIdQuery(request.PermissionId), cancellationToken);

    if (!result.IsSuccess)
    {
      Response = new ApiResponse<PermissionRecord>()
      {
        Success = false,
        Error = new ApiError
        {
          Code = result.Status.ToString(),
          Message = ErrorResponseHelper.CreateErrorsResponse(result.Errors, result.ValidationErrors)
        }
      };
      return;
    }

    Response = new ApiResponse<PermissionRecord>
    {
      Success = true,
      Data = new PermissionRecord(result.Value.Id, result.Value.EmployeeForename, result.Value.EmployeeSurname,
        result.Value.PermissionGranted,
        new PermissionTypeRecord(result.Value.PermissionType.Id, result.Value.PermissionType.Description))
    };
  }
}
