namespace Permissions.N5.Web.Permissions;

public class UpdatePermissionResponse
{
  public UpdatePermissionResponse(PermissionRecord permission)
  {
    Permission = permission;
  }

  public PermissionRecord Permission { get; set; }
}
