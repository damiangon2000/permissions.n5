namespace Permissions.N5.Web.Permissions;

public class ListPermissionsResponse
{
  public List<PermissionRecord> Permissions { get; set; } = new();
}
