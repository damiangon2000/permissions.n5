﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Permissions.N5.Core.PermissionTypeAggregate;

namespace Permissions.N5.Infrastructure.Data.Config;

public class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
{
  public void Configure(EntityTypeBuilder<PermissionType> builder)
  {
    builder.Property(p => p.Description)
      .HasMaxLength(DataSchemaConstants.DEFAULT_NAME_LENGTH)
      .IsRequired();
  }
}
