﻿using Confluent.Kafka;
using Permissions.N5.UseCases.Permissions;

namespace Permissions.N5.Infrastructure.Kafka;

public class ApacheKafkaService : IApacheKafkaService
{
  private readonly IProducer<Null, OperationMessageDTO> _producer;

   public ApacheKafkaService(IProducer<Null, OperationMessageDTO> producer)
   {
     _producer = producer;
   }

   
  public async Task ProduceMessage(string topic, OperationMessageDTO operationMessage)
  {
    var msg = new Message<Null, OperationMessageDTO> { Value = operationMessage };
    await _producer.ProduceAsync(topic, msg);
  }
  
}
