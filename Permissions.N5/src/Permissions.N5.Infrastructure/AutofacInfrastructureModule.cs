﻿using System.Reflection;
using Ardalis.SharedKernel;
using Autofac;
using Confluent.Kafka;
using Permissions.N5.Infrastructure.Data;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Configuration;
using Nest;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.Infrastructure.ElasticSearch;
using Permissions.N5.Infrastructure.Kafka;
using Permissions.N5.UseCases.Permissions;
using Permissions.N5.UseCases.Permissions.Request;
using Module = Autofac.Module;

namespace Permissions.N5.Infrastructure;

/// <summary>
/// An Autofac module responsible for wiring up services defined in Infrastructure.
/// Mainly responsible for setting up EF and MediatR, as well as other one-off services.
/// </summary>
public class AutofacInfrastructureModule : Module
{
    private readonly bool _isDevelopment = false;
    private readonly List<Assembly> _assemblies = new List<Assembly>();
    private ConfigurationManager _configurationManager;

    public AutofacInfrastructureModule(bool isDevelopment, ConfigurationManager config, Assembly? callingAssembly = null)
    {
        _isDevelopment = isDevelopment;
        _configurationManager = config;
        AddToAssembliesIfNotNull(callingAssembly);
    }

    private void AddToAssembliesIfNotNull(Assembly? assembly)
    {
        if (assembly != null)
        {
            _assemblies.Add(assembly);
        }
    }

    private void LoadAssemblies()
    {
        var coreAssembly = Assembly.GetAssembly(typeof(Permission));
        var infrastructureAssembly = Assembly.GetAssembly(typeof(AutofacInfrastructureModule));
        var useCasesAssembly = Assembly.GetAssembly(typeof(RequestPermissionCommand));

        AddToAssembliesIfNotNull(coreAssembly);
        AddToAssembliesIfNotNull(infrastructureAssembly);
        AddToAssembliesIfNotNull(useCasesAssembly);
    }

    protected override void Load(ContainerBuilder builder)
    {
        LoadAssemblies();
        if (_isDevelopment)
        {
            RegisterDevelopmentOnlyDependencies(builder);
        }
        else
        {
            RegisterProductionOnlyDependencies(builder);
        }

        RegisterEF(builder);
        RegisterQueries(builder);
        RegisterMediatR(builder);
        
    }

    private void RegisterEF(ContainerBuilder builder)
    {
        builder.RegisterGeneric(typeof(EfRepository<>))
            .As(typeof(Ardalis.SharedKernel.IRepository<>))
            .As(typeof(IReadRepository<>))
            .InstancePerLifetimeScope();
    }

    private void RegisterQueries(ContainerBuilder builder)
    {
    }

    private void RegisterMediatR(ContainerBuilder builder)
    {
        builder
            .RegisterType<Mediator>()
            .As<IMediator>()
            .InstancePerLifetimeScope();

        builder
            .RegisterGeneric(typeof(LoggingBehavior<,>))
            .As(typeof(IPipelineBehavior<,>))
            .InstancePerLifetimeScope();

        builder
            .RegisterType<MediatRDomainEventDispatcher>()
            .As<IDomainEventDispatcher>()
            .InstancePerLifetimeScope();

        var mediatrOpenTypes = new[]
        {
            typeof(IRequestHandler<,>),
            typeof(IRequestExceptionHandler<,,>),
            typeof(IRequestExceptionAction<,>),
            typeof(INotificationHandler<>),
        };

        foreach (var mediatrOpenType in mediatrOpenTypes)
        {
            builder
                .RegisterAssemblyTypes(_assemblies.ToArray())
                .AsClosedTypesOf(mediatrOpenType)
                .AsImplementedInterfaces();
        }
    }

    private void RegisterKafkaProducer(ContainerBuilder builder)
    {
      var kafkaHost = Environment.GetEnvironmentVariable("KAFKA_HOST") ?? "localhost";
      var kafkaConfig = new ProducerConfig
      {
        BootstrapServers = _configurationManager.GetSection("Kafka:BootstrapServers").Value?
          .Replace("${KAFKA_HOST}", kafkaHost),
      };
      
       var producer = new ProducerBuilder<Null, OperationMessageDTO>(kafkaConfig)
         .SetValueSerializer(new CustomValueSerializer<OperationMessageDTO>())
         .Build();
      builder.RegisterInstance(producer).As<IProducer<Null, OperationMessageDTO>>().SingleInstance();
      
      builder.RegisterType<ApacheKafkaService>()
        .As<IApacheKafkaService>()
        .InstancePerLifetimeScope();
    }

    private void RegisterElasticSearch(ContainerBuilder builder)
    {
      var envHostName = Environment.GetEnvironmentVariable("ELASTICSEARCH_HOST") ?? "localhost";
      var connectionString = _configurationManager.GetSection("Elasticsearch:Url").Value
        ?.Replace("${ELASTICSEARCH_HOST}", envHostName) ?? "http://localhost:9200";

      var settings = new ConnectionSettings(new Uri(connectionString))
        .PrettyJson()
        .DefaultIndex("permissions");

      var elasticClient = new ElasticClient(settings);
      builder.RegisterInstance(elasticClient).As<IElasticClient>().SingleInstance();
      builder.RegisterType<ElasticSearchService>()
        .As<IElasticSearchService>()
        .InstancePerLifetimeScope();
    }

    private void RegisterDevelopmentOnlyDependencies(ContainerBuilder builder)
    {
        RegisterKafkaProducer(builder);
        RegisterElasticSearch(builder);
    }

    private void RegisterProductionOnlyDependencies(ContainerBuilder builder)
    {
      
    }
}
