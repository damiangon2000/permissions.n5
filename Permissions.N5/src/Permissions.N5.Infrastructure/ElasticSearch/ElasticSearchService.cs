using Nest;
using Permissions.N5.Core.Interfaces;
using Permissions.N5.Core.PermissionAggregate;
using Permissions.N5.UseCases.Permissions;

namespace Permissions.N5.Infrastructure.ElasticSearch;

public class ElasticSearchService : IElasticSearchService
{
  private readonly IElasticClient _elasticClient;

  public ElasticSearchService(IElasticClient elasticClient)
  {
    _elasticClient = elasticClient;
  }

  public async Task InsertDocument(Permission permission)
  {
    var upser = await _elasticClient
      .UpdateAsync<Permission>(permission, u => 
        u.Doc(permission).Upsert(permission));
  }
}
