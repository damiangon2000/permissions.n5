const BASE_URL = import.meta.env.VITE_API_URL;

export async function getPermissionTypes() {
    try {
        const response = await fetch( `${BASE_URL}/PermissionTypes`);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error al obtener permisos:', error);
        throw error;
    }
}

export async function getPermissions() {
    try {
        const response = await fetch( `${BASE_URL}/Permissions`);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error al obtener permisos:', error);
        throw error;
    }
}

export async function getPermissionBy(id) {
    try {
        const response = await fetch( `${BASE_URL}/Permissions/${id}`);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error al obtener permisos:', error);
        throw error;
    }
}

export async function modifyPermission(permissionId, data) {
    try {
        console.log("modifyPermission",JSON.stringify(data))
        const response = await fetch(
            `${BASE_URL}/Permissions/${permissionId}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            }
        );
        const updatedPermission = await response.json();
        return updatedPermission;
    } catch (error) {
        console.error('Error al modificar permiso:', error);
        throw error;
    }
}

export async function requestPermission(data) {
    try {
        const response = await fetch(
            `${BASE_URL}/Permissions`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            }
        );
        const newPermission = await response.json();
        return newPermission;
    } catch (error) {
        console.error('Error al solicitar permiso:', error);
        throw error;
    }
}
