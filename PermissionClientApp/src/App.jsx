import {
    createBrowserRouter,
    Route,
    createRoutesFromElements,
    RouterProvider,
} from "react-router-dom";

import "./index.css";

import {RootLayout} from "./layouts/RootLayout";
import {ModifyPermissionPage} from "./pages/ModifyPermissionPage.jsx";
import {RequestPermissionPage} from "./pages/RequestPermissionPage.jsx";
import {ListPermissionsPage} from "./pages/ListPermissionsPage.jsx";
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment'

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<RootLayout />}>
            <Route index element={<ListPermissionsPage />} />
            <Route path="request-permission" element={<RequestPermissionPage />} />
            <Route path="modify-permission/:id" element={<ModifyPermissionPage/>} />
        </Route>
    )
);

function App() {
    return (
        <LocalizationProvider dateAdapter={AdapterMoment} adapterLocale={"es"} >
            <RouterProvider router={router}>
            </RouterProvider>
        </LocalizationProvider>

    );
}

export default App;
