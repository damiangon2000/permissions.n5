import Grid from "@mui/material/Grid";
import {useEffect, useState} from "react";
import {Box, Typography} from "@mui/material";
import {PermissionForm} from "../components/PermissionForm.jsx";
import moment from "moment";
import {getPermissionTypes, requestPermission} from "../services/api.js";
import { useNavigate } from "react-router-dom";

const RequestPermissionPage = () => {
    const [permission, setPermission] = useState({
        id: 0,
        employeeForename: "",
        employeeSurname: "",
        permissionGranted: moment().format(),
        permissionTypeId: 0
    });
    const [permissionTypes, setPermissionTypes] = useState([]);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();

    useEffect( () => {
        async function fetchPermissionTypes() {
            try {
                const data = await getPermissionTypes();
                if (data.success) {
                    setPermissionTypes(data.data.permissionTypes);
                }else {
                    console.log(data.error)
                }
            } catch (error) {
                console.error('Error al obtener permisos:', error);
            }
        }

        setLoading(true)
        fetchPermissionTypes().finally(() => setLoading(false));

    }, []);

    const onFormSubmitHandler = async (data) => {
        try {
            const result = await requestPermission(data);
            if (result.success) {
                navigate("/");
            }else {
                console.log(data.error)
            }
        } catch (error) {
            console.error('Error al obtener permisos:', error);
        }

    }

    return (
        <>
            <Grid container justifyContent="center">
                <Box sx={{  textAlign: "center" }}>
                    <Typography variant="h4" component="h2" gutterBottom>
                        Request
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary">
                        Request new permission
                    </Typography>
                </Box>
                <Grid container>
                    {!loading && <PermissionForm permission={permission} permissionTypes={permissionTypes} onFormSubmit={onFormSubmitHandler} /> }
                </Grid>
            </Grid>
        </>
    );
};

export {RequestPermissionPage};
