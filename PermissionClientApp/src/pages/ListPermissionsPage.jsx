import {
    Box,
    Grid,
    Typography,
} from "@mui/material";

import {PermissionItem} from "../components/PermissionItem.jsx";
import {useEffect, useState} from "react";
import {getPermissions} from "../services/api.js";

const ListPermissionsPage = () => {
    const [permissions, setPermissions] = useState([]);

    useEffect( () => {
        async function fetchData() {
            try {
                const data = await getPermissions();
                if (data.success) {
                    setPermissions(Array.from(data.data.permissions));
                }else {
                    console.log(data.error)
                }
            } catch (error) {
                console.error('Error al obtener permisos:', error);
            }
        }
        fetchData();

    }, []);
    return (
        <>
            <Grid container justifyContent="center">
                <Box sx={{  textAlign: "center" }}>
                    <Typography variant="h4" component="h2" gutterBottom>
                        Get Permissions
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary">
                        List all permissions
                    </Typography>
                </Box>
                <Grid container spacing={4}>
                    {permissions.map((permission, index) => (
                        <PermissionItem key={index} permission={permission}/>
                    ))}
                </Grid>
            </Grid>
        </>
    );
}

export  {ListPermissionsPage};
