import {useParams} from "react-router-dom";
import {PermissionForm} from "../components/PermissionForm.jsx";
import {useEffect, useState} from "react";
import {getPermissionBy, getPermissionTypes, modifyPermission} from "../services/api.js";
import {Box, Grid, Typography} from "@mui/material";
import { useNavigate } from "react-router-dom";

const ModifyPermissionPage = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const [permission, setPermission] = useState(undefined);
    const [permissionTypes, setPermissionTypes] = useState([]);
    const [loading, setLoading] = useState(true);



    useEffect( () => {
        async function fetchPermissionTypes() {
            try {
                const data = await getPermissionTypes();
                if (data.success) {
                    setPermissionTypes(data.data.permissionTypes);
                }else {
                    console.log(data.error)
                }
            } catch (error) {
                console.error('Error al obtener permisos:', error);
            }
        }
        async function fetchData() {
            try {
                const data = await getPermissionBy(id);
                if (data.success) {
                    setPermission({
                        id: data.data.id,
                        employeeForename: data.data.employeeForename,
                        employeeSurname: data.data.employeeSurname,
                        permissionGranted: data.data.permissionGranted,
                        permissionTypeId: data.data.permissionType.id
                    });
                }else {
                    console.log(data.error)
                }
            } catch (error) {
                console.error('Error al obtener permisos:', error);
            }
        }
        setLoading(true)
        fetchData().finally(() => setLoading(false));
        fetchPermissionTypes().finally(() => setLoading(false));

    }, []);

    const onFormSubmitHandler = async (formData) => {
        try {
            delete formData.id;
            const result = await modifyPermission(id, formData);
            if (result.success) {
                navigate("/");
            } else {
                console.log(formData.error)
            }
        } catch (error) {
            console.error('Error al obtener permisos:', error);
        }
    }

    return (
        <>
            <Grid container justifyContent="center">
                <Box sx={{  textAlign: "center" }}>
                    <Typography variant="h4" component="h2" gutterBottom>
                        Modify
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary">
                        Edit permission
                    </Typography>
                </Box>
                <Grid container>
                    {!loading && permission && <PermissionForm  permission={permission} permissionTypes={permissionTypes} onFormSubmit={onFormSubmitHandler} /> }
                </Grid>
            </Grid>
        </>
    );
}

export {ModifyPermissionPage};
