import { Box, Card, CardContent, Grid, Typography} from "@mui/material";
import Button from "@mui/material/Button";
import * as React from "react";

// eslint-disable-next-line react/prop-types
const PermissionItem = ({permission}) => {

        return(
            <>
                <Grid item xs={12} md={6}>
                    <Card sx={{display: "flex"}}>
                        <Box sx={{display: "flex", flexDirection: "column"}}>
                            <CardContent sx={{flex: "1 0 auto"}}>
                                <Typography component="h5" variant="h5">
                                    {permission.id}
                                </Typography>
                            </CardContent>
                            <Box sx={{display: "flex", alignItems: "center", p: 1}}>
                                <Box sx={{ml: 1}}>
                                    <Typography variant="subtitle2">{permission.employeeForename} {permission.employeeSurname}</Typography>
                                    <Typography variant="subtitle2" color="text.secondary">
                                        {permission.permissionGranted}
                                    </Typography>
                                    <Typography variant="subtitle2" color="text.secondary">
                                        PermissionType: {permission.permissionType.id} - {permission.permissionType.description}
                                    </Typography>
                                    <Button href={`modify-permission/${permission.id}`} variant="outlined">Modify</Button>
                                </Box>
                            </Box>
                        </Box>
                    </Card>
                </Grid>
            </>
        );
}

export {PermissionItem};
