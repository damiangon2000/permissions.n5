import * as React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { useState} from "react";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import moment from "moment";
import {InputLabel, MenuItem, Select, Stack} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';

const PermissionForm = ({permission, permissionTypes, onFormSubmit} ) => {

    const [formData, setFormData] = useState(permission);

    const handleSubmit = (event) => {
        event.preventDefault();
        onFormSubmit(formData);
    };

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleDateChange = (e) => {
        setFormData({ ...formData, permissionGranted: e.format() });
    }

    return (
        <>
            <Container component="main">
                <CssBaseline />
                <Box
                    sx={{
                        mt: "8",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                    }}
                >
                    <Box
                        component="form"
                        noValidate
                        onSubmit={handleSubmit}
                        sx={{ mt: 3 }}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    name="employeeForename"
                                    required
                                    fullWidth
                                    id="employeeForename"
                                    label="Forename"
                                    autoFocus
                                    onChange={handleChange}
                                    value={formData.employeeForename}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="employeeSurname"
                                    label="Surname"
                                    name="employeeSurname"
                                    onChange={handleChange}
                                    value={formData.employeeSurname}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <DatePicker
                                    label="Permission Granted"
                                    id="permissionGranted"
                                    name="permissionGranted"
                                    onChange={handleDateChange}
                                    value={moment(formData.permissionGranted)}
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <InputLabel id="permissionTypeLabel"> Permission Type </InputLabel>
                                <Select
                                    labelId="permissionTypeLabel"
                                    id="permissionTypeId"
                                    name="permissionTypeId"
                                    value={formData.permissionTypeId}
                                    label="Permission Type"
                                    onChange={handleChange}
                                >
                                    <MenuItem key={-1} value={0}>Select...</MenuItem>

                                    {permissionTypes.map((item, index) => (
                                        <MenuItem key={index} value={item.id}>{item.description}</MenuItem>
                                    ))}
                                </Select>
                            </Grid>

                            <Grid item xs={12}>
                                <Stack direction="row" spacing={2}>
                                    <Button variant="outlined" startIcon={<DeleteIcon />} sx={{ mt: 3, mb: 2 }} href={"/"}>
                                        Cancel
                                    </Button>

                                    <Button
                                        type="submit"

                                        variant="contained"
                                        sx={{ mt: 3, mb: 2 }}
                                        endIcon={<SendIcon />}>
                                        Save
                                    </Button>
                                </Stack>

                            </Grid>
                        </Grid>

                    </Box>
                </Box>
            </Container>
        </>
    )
}

export {PermissionForm};
